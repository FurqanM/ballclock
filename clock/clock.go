package clock

import (
	"ballclock/ball"
	"ballclock/ballholders"
	"fmt"
	"math"
	"strings"
)

// static ballholder capacities
const hourRailCap = 11
const fiveMinRailCap = 11
const oneMinRailCap = 4

var queue ballholders.Queue
var hourRail ballholders.Rail
var fiveMinRail ballholders.Rail
var oneMinRail ballholders.Rail
var nClockRefreshes uint64

func init() {
	hourRail = ballholders.NewRail(hourRailCap)
	fiveMinRail = ballholders.NewRail(fiveMinRailCap)
	oneMinRail = ballholders.NewRail(oneMinRailCap)
}

// Update the clock state by adding ball
func updateClockState(b ball.Ball) {
	var spilledBalls []ball.Ball

	spilledBalls = oneMinRail.Push(b)
	if len(spilledBalls) == 0 {
		return
	}
	queue.Push(spilledBalls)

	spilledBalls = fiveMinRail.Push(b)
	if len(spilledBalls) == 0 {
		return
	}
	queue.Push(spilledBalls)

	spilledBalls = hourRail.Push(b)
	if len(spilledBalls) == 0 {
		return
	}
	queue.Push(append(spilledBalls, b))
}

// Detect a cycle occurrence in a ball clock and track time for that cycle to
// occur
func findCycle(queueCapacity uint8) {
	// break when the balls are all back in their original positions in the
	// queue
	for {
		ball := queue.Pop()
		updateClockState(ball)
		if queue.IsFull() {
			nClockRefreshes++
			if queue.DoCycleCheck() {
				break
			}
			// These printlines display the balls thare are in the rails
			//			fmt.Println(hourRail)
			//			fmt.Println(fiveMinRail)
			//			fmt.Println(oneMinRail)
			// This displays the amount of times the clock has refreshed (2 per day)
			// Ones every 12 hours
			//			fmt.Println(nClockRefreshes)
		}
	}
}

//func minuteCountdownCycle(queueCapacity uint8, goal uint8) uint64 {

//	// break out of this loop when we reach our desired outcome of the amount of
//	// minutes passed into the second argument
//	for GetDaysUntilCycle(queueCapacity) == goal {
//		//TODO Countdown or count up and stop when the number of days or cycles is
//		//equal to the amount of hours that is expected to run
//	}
//}

// This will return the amount of minutes that the clock has gone through
func GetMinutesUntilCycle(queueCapacity uint8) uint64 {
	// Get the number of times the clock refreshes which is 12-hour
	// periods
	nClockRefreshes = 0
	queue = ballholders.NewQueue(queueCapacity)
	findCycle(queueCapacity)
	// 24 hours in a day, and 2 clock refreshes a day
	return uint64((math.Ceil(float64(nClockRefreshes/2)) * 24))

}

func GetDaysUntilCycle(queueCapacity uint8) uint64 {
	// Number of times the clock refreshes, i.e., the number of 12-hour
	// periods
	nClockRefreshes = 0
	queue = ballholders.NewQueue(queueCapacity)
	findCycle(queueCapacity)
	// There 2 clock refreshes in a day
	return uint64(math.Ceil(float64(nClockRefreshes) / 2.0))
}

func GetStateAfterXMinutes(queueCapacity uint8, minutes uint64) string {

	//Initializing All Rails and Queues for each new processing
	hourRail = ballholders.NewRail(hourRailCap)
	fiveMinRail = ballholders.NewRail(fiveMinRailCap)
	oneMinRail = ballholders.NewRail(oneMinRailCap)
	queue = ballholders.NewQueue(queueCapacity)

	i := uint64(0)

	//Clock Operation for Given Number of Minutes
	for i < minutes {
		ball := queue.Pop()
		updateClockState(ball)
		i++
	}

	//Building Return Message in JSON Format with Values of All Rails plus the Queue
	msg := fmt.Sprintf("\"Min\":%v,\"FiveMin\":%v,\"Hour\":%v,\"Main\":%v", oneMinRail.GetRailValue(), fiveMinRail.GetRailValue(), hourRail.GetRailValue(), queue.GetQueueValue())
	return strings.Replace(msg, " ", ",", -1)
}
