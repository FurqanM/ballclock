package main

import (
	"ballclock/clock"
	"bufio"
	"errors"
	"flag"
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
)

const nArgs = 0
const maxBalls = 127
const minBalls = 27
const endOfInputVal = 0

//checks the arguments passed in from OS
func usage() {
	name := path.Base(os.Args[0])
	msg := fmt.Sprintf("Usage: %s\n\n"+
		"%s takes no arguments and accepts input from stdin.\n", name, name)
	fmt.Fprintf(os.Stderr, msg)
}

//parses the command line
func parseCommandLine() {
	flag.Parse()
}

// Take bufio Scanner then parse the scanned input.
// If an error is returned then there is a problem parsing the input.
func run(scanner *bufio.Scanner, file *os.File, validateInputOnly bool) error {
	// Only need a uint8, but the strconv.ParseUint() returns a unsignedInt 64.
	var nBalls uint64
	var nTime uint64
	var err error

	for scanner.Scan() {
		// parsed value is base 10 and should fit within 8 bits
		text := scanner.Text()

		num := strings.Split(text, " ")

		if len(num) == 1 {
			if nBalls, err = strconv.ParseUint(num[0], 10, 8); err != nil {
				msg := fmt.Sprintf("bad input (failed to parse \"%s\" as uint8)", text)
				fmt.Fprintf(os.Stderr, msg)
				return errors.New(msg)
			}
			if nBalls == endOfInputVal {
				return nil
			} else if nBalls > maxBalls {
				msg := fmt.Sprintf("bad input (Too many balls, %d > %d)", nBalls, maxBalls)
				fmt.Fprintln(os.Stderr, msg)
				return errors.New(msg)
			} else if nBalls < minBalls {
				msg := fmt.Sprintf("bad input (Not enough balls, %d < %d)", nBalls, minBalls)
				fmt.Fprintln(os.Stderr, msg)
				return errors.New(msg)
			}
			if !validateInputOnly {
				fmt.Fprintf(file, "%d balls cycle after %d days.\n",
					nBalls,
					clock.GetDaysUntilCycle(uint8(nBalls)))
				// This will return the amount of minutes that the clock has gone through
				fmt.Println(clock.GetMinutesUntilCycle(uint8(nBalls))) // TODO remember to remove this when you're done
				fmt.Fprintf(os.Stderr, "\n\n")
			}
		} else if len(num) == 2 {

			if nBalls, err = strconv.ParseUint(num[0], 10, 8); err != nil {
				msg := fmt.Sprintf("bad input (failed to parse \"%s\" as uint8)", text)
				fmt.Fprintf(os.Stderr, msg)
				return errors.New(msg)
			}
			if nTime, err = strconv.ParseUint(num[1], 10, 64); err != nil {
				msg := fmt.Sprintf("Bad input (failed to parse \"%s\" as uint8)", text)
				fmt.Fprintf(os.Stderr, msg)
				return errors.New(msg)
			}

			msg := clock.GetStateAfterXMinutes(uint8(nBalls), nTime)
			fmt.Fprintf(os.Stderr, msg)
			fmt.Fprintf(os.Stderr, "\n\n")
		} else {
			msg := fmt.Sprintf("Bad Input 1 or two Arguments Expected ")
			fmt.Fprintf(os.Stderr, msg)
			return errors.New(msg)
		}

	}
	if err = scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading from input:", err.Error())
		return err
	} else if nBalls == 0 {
		msg := "bad input (empty)"
		fmt.Fprintln(os.Stderr, msg)
		return errors.New(msg)
	} else if nBalls != 0 {
		msg := fmt.Sprintf("bad input (zero should signify the end of input, got %d)", nBalls)
		fmt.Fprintln(os.Stderr, msg)
		return errors.New(msg)
	}
	return nil
}

func main() {
	flag.Usage = usage
	parseCommandLine()
	if flag.NArg() != nArgs {
		usage()
		os.Exit(1)
	}

	// The input may be of an unspecified length, so we'll use buffered IO
	// and compute the ball cycles as we receive input
	if err := run(bufio.NewScanner(os.Stdin), os.Stdout, false); err != nil {
		os.Exit(1)
	}
}
