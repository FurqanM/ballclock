//Package ball of the clock
package ball

// Ball is the creation of a new Ball struct
type Ball struct {
	// The original position of the ball in a ball holder
	ID uint8
}

// New creates a new ball with a uint8 value
func New(id uint8) Ball {
	return Ball{id}
}
