package ball

import (
	"ballclock/ball"
	"testing"
)

func TestNewBall(t *testing.T) {
	const BallID = 0
	ball := ball.New(BallID)
	// var theBall = ball.Ball{}
	if ball.ID != BallID {
		t.Errorf("Unexpected ball ID (Actual: %d, Expected: %d)\n", ball.ID, BallID)
	}
}
